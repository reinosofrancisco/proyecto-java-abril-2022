# Proyecto Java Abril 2022

Link a la clase: <br>
https://utn.zoom.us/j/82849460146?pwd=TnJOR004U2RDY09zMU5XQWdyclVyZz09
Passcode = **TnJOR004U2RDY09zMU5XQWdyclVyZz09**


### En este repositorio se van a adjuntar los ejercicios de la clases.

Cada ejercicio se encuentra en un archivo separado, y cada practica esta
dividida en un paquete con el nombre de dicha practica.

El path es `../src`

El formato de los ejercicios es `<NombreEjercicio>_ReinosoFrancisco.java`
