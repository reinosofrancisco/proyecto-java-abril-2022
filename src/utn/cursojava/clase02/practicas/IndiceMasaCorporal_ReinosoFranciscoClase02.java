package utn.cursojava.clase02.practicas;

import java.util.Scanner;

public class IndiceMasaCorporal_ReinosoFranciscoClase02 {

    /**Nota: Podria utilizar funciones 'static' para evitar la instanciacion de la clase */
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        float A,B;
        do {
            System.out.println("Ingrese el Peso en Kilogramos (>0): ");
            A = scanner.nextFloat();
            if(A<=0){
                System.out.println("Peso Invalido");
            }
            System.out.println("Ingrese la Altura en Metros (>0): ");
            B = scanner.nextFloat();
            if(B<=0){
                System.out.println("Altura Invalida");
            }
        }while(A<=0 || B<=0);

        IndiceMasaCorporal_ReinosoFranciscoClase02 imc = new IndiceMasaCorporal_ReinosoFranciscoClase02();

        float imc_resultado = imc.IMC(A,B);
        System.out.println("El Indice de Masa Corporal es: " + imc_resultado);
        System.out.println("El Nivel de Peso es: " + imc.nivel_de_peso(imc_resultado));
    }

    public IndiceMasaCorporal_ReinosoFranciscoClase02() {
    }

    public float IMC(float peso, float altura){
        return peso/(altura*altura);
    }

    public String nivel_de_peso(float imc){
        String nivel = "";

        if(imc<18.5){
            nivel = "Bajo Peso";
        }
        else if(imc>=18.5 && imc<25){
            nivel = "Normal";
        }
        else if(imc>=25 && imc<30){
            nivel = "Sobrepeso";
        }
        else if(imc>=30 && imc<35){
            nivel = "Obesidad Tipo I";
        }
        else if(imc>=35 && imc<40){
            nivel = "Obesidad Tipo II";
        }
        else if(imc>=40){
            nivel = "Obesidad Tipo III";
        }
        return nivel;

    }


}
