package utn.cursojava.clase02.practicas;

import java.util.Scanner;

public class Calculadora_ReinosoFranciscoClase02 {

    /**Nota: Podria utilizar funciones 'static' para evitar la instanciacion de la clase */
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        int A,B;
        System.out.println("Ingrese el primer numero + Enter");
        A = scanner.nextInt();
        System.out.println("Ingrese el segundo numero + Enter");
        B = scanner.nextInt();

        Calculadora_ReinosoFranciscoClase02 calc = new Calculadora_ReinosoFranciscoClase02();

        System.out.println("A = " + A + " B = " + B);
        System.out.println( "Suma " + calc.suma(A,B) );
        System.out.println( "Resta " + calc.resta(A,B) );
        System.out.println( "Multiplic " + calc.multiplicacion(A,B) );
        System.out.println( "Division " + calc.division(A,B) );
        System.out.println( "Resto " + calc.residuo(A,B) );



        scanner.close();
    }

    /** Constructor Vacio */
    public Calculadora_ReinosoFranciscoClase02() {
    }

    /** Operaciones Aritmeticas*/

    public int suma(int a, int b){
        return(a+b);
    }

    public int resta(int a,int b){
        return(a-b);
    }

    public int multiplicacion(int a,int b){
        return(a*b);
    }

    public double division(double a,double b){
        return(a/b);
    }

    public int residuo(int a,int b){
        return(a%b);
    }



}
