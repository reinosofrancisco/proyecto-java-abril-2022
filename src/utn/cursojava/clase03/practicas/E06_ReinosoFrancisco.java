package utn.cursojava.clase03.practicas;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class E06_ReinosoFrancisco {

    /**
     * 6) Crear un programa Java que imprima la siguiente tabla:
     *
     * Nota 1 | Nota 2 | Nota 3 | Promedio
     * 8     | 7      | 5       | 6,66
     *
     * a) Ingrese la cantidad de filas a mostrar. (COL = 4) [Done]
     * b) Cargue el valor de las notas utilizando una función Random. [Done]
     * c) Calcule el promedio de las tres notas por cada fila. [Done]
     * d) Muestre el promedio por fila redondeando a 2 cifras decimales. [Done]
     * */

    public static void main(String[] args) {

        int rows;
        Scanner scanner = new Scanner(System.in);

        try{
            System.out.println("Ingrese el valor de filas a generar");
            rows = scanner.nextInt();

            printTable(rows);
        } catch (InputMismatchException e) {
            System.out.println("Error: " + e.getMessage() + "\t | Can't input string");
        } finally {
            scanner.close();
        }

    }

    /**
     * Función que genera una tabla de notas
     * @param rows Cantidad de filas a generar
     * */
    public static void printTable(int rows){
        Random random = new Random();
        int a, b, c;
        double prom;

        System.out.print("-------------------------------------------\n");
        for (int i = 0; i < rows; i++) {
            a = random.nextInt(10);
            b = random.nextInt(10);
            c = random.nextInt(10);

            /* Calculo el promedio */
            prom = (double)(a + b + c) / 3;

            /* Redondear promedio a 2 decimales
             * - prom = Math.round(prom*100.0)/100.0;
             * - De preferencia, utilice directamente "%.2f" para redondear a 2 decimales.
             * y asi evitar modificar la variable prom.
             * */

            /* Print the table */
            System.out.print("Nota 1 \t | Nota 2  \t | Nota 3  \t | Promedio\n");
            System.out.print( a + "\t\t | " + b + "\t\t | "  + c + "\t\t | ");
            System.out.printf("%.2f", prom);
            System.out.print("\n");
            System.out.print("-------------------------------------------\n");
        }
        System.out.println("*** Ejecucion finalizada ***");
    }
}
