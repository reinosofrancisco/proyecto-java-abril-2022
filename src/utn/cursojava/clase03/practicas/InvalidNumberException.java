package utn.cursojava.clase03.practicas;

public class InvalidNumberException extends Exception {
    private static final long serialVersionUID = 1L;

    // Empty constructor just in case
    public InvalidNumberException() {
        super("Number is invalid");
    }
    public InvalidNumberException(String message) {
        super(message);
    }

}
