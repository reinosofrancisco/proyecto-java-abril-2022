package utn.cursojava.clase03.practicas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class E03_ReinosoFrancisco {

    /**
     * 3) Crear un programa Java para calcular el resto y cociente de dos números
     * por medio de restas sucesivas.
     * */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num1,num2;

        try{
            System.out.println("Ingrese el primer numero + Enter");
            num1 = scanner.nextInt();

            System.out.println("Ingrese el segundo numero + Enter");
            num2 = scanner.nextInt();

            int[] res = restaSucesiva(num1,num2);

            System.out.println("El Resultado de " + num1 + " div " + num2 + " es: " + res[0] + " y el resto es: " + res[1]);

        } catch (InputMismatchException e) {
            System.out.println("Error: " + e.getMessage() + "\t | Can't input string");
        } finally {
            scanner.close();
        }


    }

    /**
     * Calculo de la Resta sucesiva
     * @param num1 int > num2
     * @param num2 int
     * @return int[] con el resultado y el resto
     * */
    public static int[] restaSucesiva(int num1, int num2){

        int[] res = new int[2];
        res[0] = 0; // cociente

        /* Mientras Num1 Sea mayor o igual que Num2, aumentamos res */
        while( num1 >= num2){
            num1 -= num2;
            res[0]++;
        }

        res[1] = num1; // resto

        return res;
    }

}
