package utn.cursojava.clase03.practicas;

public class NegativeNumberException extends Exception {

    public NegativeNumberException() {
        super("Number can't be negative");
    }

}
