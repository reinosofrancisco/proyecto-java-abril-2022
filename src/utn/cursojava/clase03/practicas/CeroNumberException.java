package utn.cursojava.clase03.practicas;

public class CeroNumberException extends Exception {

    public CeroNumberException() {
        super("Number can't be zero");
    }

    public CeroNumberException(String message) {
        super(message);
    }



}
