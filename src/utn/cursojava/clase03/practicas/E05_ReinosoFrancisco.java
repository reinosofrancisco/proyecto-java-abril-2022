package utn.cursojava.clase03.practicas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class E05_ReinosoFrancisco {

    /**
     * 5) Crear un programa Java para obtener el M.C.D de un número por medio del
     * algoritmo de Euclides. (M.C.D = Máximo Común Divisor)
     */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num1,num2;

        try {
            System.out.println("Ingrese el primer numero + Enter");
            num1 = scanner.nextInt();

            System.out.println("Ingrese el segundo numero + Enter");
            num2 = scanner.nextInt();

            System.out.println("M.C.D de a y b: " + mcd(num1, num2));
        } catch (InputMismatchException e) {
            System.out.println("Error: " + e.getMessage() + "\t | Can't input string");
        } finally {
            scanner.close();
        }

    }


    /**
     * Método que calcula el M.C.D de dos números
     * Ejemplo: mcd(1032, 108) = 12
     * En caso de que alguno de los números sea 0, el M.C.D es 0
     *
     * @param a Primer número
     * @param b Segundo número != 0
     * @return M.C.D
     */
    public static int mcd(int a, int b) {
        int r,acm;

        if ((a > 0 ) && (b > 0)) {
            /*Intercambio el orden para que a > b */
            if (b > a) {
                acm = b;
                b = a;
                a = acm;
            }

            r = a % b;

            while (r > 0) {
                acm = b;
                b = r;
                a = acm;
                r = a % b;
            }
        }
        else b = 0;

        /* El valor de b contiene el MCD*/
        return b;
    }


}
