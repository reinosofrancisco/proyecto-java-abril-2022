package utn.cursojava.clase03.practicas;

public class E01_ReinosoFrancisco {

    /**
     * 1) Crear un programa Java para calcular la suma de los primeros 100 numeros
     * enteros positivos con un ciclo for, mientras y repetir.
     * */

    public static void main(String[] args) {
        E01_ReinosoFrancisco e01 = new E01_ReinosoFrancisco();
        System.out.println("Suma de los primeros 100 numeros enteros positivos con for: " + e01.primeros100NumerosEnterosPositivosFor());
        System.out.println("Suma de los primeros 100 numeros enteros positivos con while: " + e01.primeros100NumerosEnterosPositivosMientras());
        System.out.println("Suma de los primeros 100 numeros enteros positivos con do while: " + e01.primeros100NumerosEnterosPositivosRepetir());
    }

    public int primeros100NumerosEnterosPositivosFor(){
        int suma = 0;
        for (int i = 0; i < 100; i++) {
            suma += i;
        }
        return suma;
    }



    public int primeros100NumerosEnterosPositivosMientras(){
        int suma = 0;
        int i = 0;
        while (i < 100) {
            suma += i;
            i++;
        }
        return suma;
    }

    public int primeros100NumerosEnterosPositivosRepetir(){
        int suma = 0;
        int i = 0;
        do {
            suma += i;
            i++;
        } while (i < 100);
        return suma;
    }
}