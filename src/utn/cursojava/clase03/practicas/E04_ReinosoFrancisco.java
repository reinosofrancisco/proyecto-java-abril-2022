package utn.cursojava.clase03.practicas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class E04_ReinosoFrancisco {

    /** 4) Crear un programa Java para calcular la Serie de Fibonnaci. */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num1;
        System.out.println("Ingrese el valor a calcular la serie de Fibonacci");

        try {
            num1 = scanner.nextInt();
            System.out.println("Fibonacci de " + num1 + " es " + fibonacci(num1));
            System.out.println("Fibonacci de " + num1 + " con For Loop es " + fibonacciFor(num1));
        } catch (InputMismatchException e) {
            System.out.println("Error: " + e.getMessage() + "\t | Can't input string");
        } finally {
            scanner.close();
        }

    }

    /**
     * Método que calcula la serie de Fibonacci de un número dado
     * utilizando Recursividad.
     * Ejemplo: Fibonacci(32) = 2178309
     *
     * @param n int
     * @return número de la serie de Fibonacci.
     * */
    public static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /** Metodo que calcula la serie de Fibonacci con un For Loop.
     * Ejemplo: Fibonacci(32) = 2178309
     *
     * @param n int
     * @return número de la serie de Fibonacci.
     * */
    public static int fibonacciFor(int n) {
        int a = 0;
        int b = 1;
        int c = 0;

        for (int i = 0; i < n - 1; i++) {
            c = a + b;
            a = b;
            b = c;
        }

        /* Si n=1 la serie debe retornar 1 */
        c = (n == 1) ? 1 : c;

        return c;
    }
}
