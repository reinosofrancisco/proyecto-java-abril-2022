package utn.cursojava.clase03.practicas;

public class E02_ReinosoFrancisco {

    /**
     * 2) Crear un programa Java que imprima la tabla de multiplicar de los números
     * 1 al 9.
     * */

    public static void main(String[] args) {
        for (int i = 1; i <= 9; i++) {
            tablaMultiplicar(i);
        }
    }

    /**
     * @param numero entero >= 0
     * */
    public static void tablaMultiplicar(int numero){
        for (int i = 1; i <= 10; i++) {
            System.out.print(numero + " x " + i + " = " + numero * i + " | ");
        }
        System.out.println();
    }
}
