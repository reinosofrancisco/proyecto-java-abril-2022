package utn.cursojava.clase03.practicas;

public class E08_ReinosoFrancisco {

    /** 8) Crear un programa Java que dibuje la siguiente pirámide:
     * * * * * * * * * *
     *  * * * * * * * *
     *   * * * * * * *
     *    * * * * * *
     *     * * * * *
     *      * * * *
     *       * * *
     *        * *
     *         *
     * En este ejemplo, la altura de la pirámide es de 9.
     * */

    public static void main(String[] args) {
        piramideInvertida(9);
    }

    /** Imprime una piramide invertida de altura n
     * @param n número de filas
     * */
    public static void piramideInvertida(int n){
        for (int i = 0; i < n; i++) {

            for (int j = 0; j < i; j++) {
                System.out.print(" ");
            }

            for (int j = 0; j < n-i; j++) {
                System.out.print("* ");
            }

            System.out.println();
        }
    }
}
