package utn.cursojava.clase03.practicas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class E07_ReinosoFrancisco {

    /**
     * ) Crear un programa Java que permita calcular el factorial de un numero N.
     * para realizar el calculo utilice todas las estructuras repetitivas estudiadas.
     * Se llama factorial de un número natural n al producto de los n primeros
     * números naturales Se representa por n!, es decir, el factorial de n (n!) se
     * calcula como:
     * n!=n x (n-1) x (n-2) x ........ 3 x 2 x 1.
     *
     * Por ejemplo, el factorial de 5 es 5!=5 x 4 x 3 x 2 x 1 -> 5!=120 Se define la
     * factorial de 0 por 0! = 1.
     *
     */

    public static void main(String[] args) {

        int numero;
        Scanner scanner = new Scanner(System.in);


        try{
            System.out.println("Ingrese el valor a calcular: ");
            numero = scanner.nextInt();

            /* El mayor valor a ingresar sera 31, luego de eso no puedo representar el resultado con int.
             * Integer.MAX_VALUE = 2147483647 */
            System.out.println("factorial(" + numero + ") Recursivo= " + factorialRecursivo(numero));
            System.out.println("factorial(" + numero + ") While= " + factorialWhile(numero));
            System.out.println("factorial(" + numero + ") DoWhile= " + factorialDoWhile(numero));
            System.out.println("factorial(" + numero + ") For= " + factorialFor(numero));

        } catch (InputMismatchException e) {
            System.out.println("Error: " + e.getMessage() + "\t | Can't input string");
        } finally {
            scanner.close();
        }
    }

    /** Metodo que calcula el factorial de un numero de forma Recursiva
     * @param n numero natural
     * */
    public static int factorialRecursivo(int n){
        if(n == 0){
            return 1;
        }
        return n * factorialRecursivo(n-1);
    }

    /** Metodo que calcula el factorial de un numero utilizando while
     * @param n numero natural
     * */
    public static int factorialWhile(int n){
        int factorial = 1;
        while(n > 0){
            factorial *= n;
            n--;
        }
        return factorial;
    }

    /** Metodo que calcula el factorial de un numero utilizando do while
     * @param n numero natural
     * */
    public static int factorialDoWhile(int n){
        int factorial = 1;
        /* Si n=0 el factorial 0! = 1 */
        if (n > 0) {
            do {
                factorial *= n;
                n--;
            } while (n > 0);
        }
        return factorial;
    }

    /** Metodo que calcula el factorial de un numero utilizando for
     * @param n numero natural
     * */
    public static int factorialFor(int n){
        int factorial = 1;
        /* Se puede hacer de (i = 1) hasta (i <= n.) Es indiferente */
        for(int i = n; i > 0; i--){
            factorial *= i;
        }
        return factorial;
    }
}
